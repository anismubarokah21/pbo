import java.util.Scanner;

public class PembayaranTiket {
    private double harga;
    private double saldo;
    private Scanner scanner;

    public PembayaranTiket(double harga, double saldo) {
        this.harga = harga;
        this.saldo = saldo;
        scanner = new Scanner(System.in);
    }
    public boolean bayar() {
        if (saldo < harga) {
            System.out.println("Saldo Anda tidak mencukupi.");
            return false;
        }
        System.out.print("Masukkan PIN: ");
        int pin = scanner.nextInt();
        if (pin != 1234) {
            System.out.println("PIN salah.");
            return false;
        }
        saldo -= harga;
        System.out.println("Pembayaran berhasil. Sisa saldo: " + saldo);
        return true;
    }
    
    public double setSaldo(double saldo) {
        return this.saldo = saldo;
    }

    public double getSaldo() {
        return saldo;
    }
}