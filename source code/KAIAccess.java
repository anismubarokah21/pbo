import java.util.Scanner;
import java.time.LocalTime;

public class KAIAccess {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan username: ");
        String username = input.nextLine();
        System.out.print("Masukkan password: ");
        String password = input.nextLine();

        Authenticator authenticator = new Login("Anis", "anis123");
        boolean isAuthenticated = authenticator.authenticate(username, password);

        if (isAuthenticated) {
            System.out.println("Berhasil Login!");
        } else {
            System.out.println("Username atau password salah! Silakan coba lagi.");
            System.exit(0);
        }
        
        int pil;
        jadwaltiket jadwaltiket1 = new jadwaltiket();
        HargaTiket hargaTiket1 = new HargaTiket();
        PembayaranTiket pembayaranTiket1 = new PembayaranTiket(0.0, 1000000);
        pesantiket pesantiket1 = null;
        Jadwal jadwalPemesanan = null;
        double hargaPemesanan = 0.0;

        while (true) {
            System.out.println("Selamat Datang di Aplikasi KAI Access");
            System.out.println("1. Melihat Jadwal Tiket");
            System.out.println("2. Melihat Harga Tiket");
            System.out.println("3. Pemesanan Tiket");
            System.out.println("4. Pembayaran Tiket");
            System.out.println("5. Keluar");
            System.out.print("Pilihan: ");
            pil = input.nextInt();

            switch (pil) {
                case 1:
                    System.out.println("Melihat Jadwal Tiket");
                    jadwaltiket1.tambahJadwal("KA 123", "Jakarta", "Surabaya", LocalTime.of(8, 0), LocalTime.of(16, 0));
                    jadwaltiket1.tambahJadwal("KA 456", "Surabaya", "Jakarta", LocalTime.of(9, 0), LocalTime.of(17, 0));
                    jadwaltiket1.tambahJadwal("KA 213", "Bandung", "Banjar", LocalTime.of(14, 0), LocalTime.of(18, 0));
                    jadwaltiket1.tambahJadwal("KA 215", "Banjar", "Bandung", LocalTime.of(22, 0), LocalTime.of(3, 0));
                    jadwaltiket1.tampilJadwal();
                    break;

                case 2:
                    System.out.println("Melihat Harga Tiket");
                    hargaTiket1.tampilharga();
                    break;

                case 3:
                    System.out.println("Pemesanan Tiket");
                    System.out.print("Masukkan kode jadwal tiket: ");
                    input.nextLine();
                    String kodeJadwal = input.nextLine();
                    Jadwal jadwal = jadwaltiket1.getJadwalByKode(kodeJadwal);
                    if (jadwal != null) {
                        System.out.print("Masukkan kelas tiket: ");
                        String kelasTiket = input.nextLine();
                        double hargaTiket = hargaTiket1.getHarga(kelasTiket);
                        if (hargaTiket > 0) {
                            System.out.println("Pemesanan tiket berhasil!");
                            System.out.println("Jadwal Tiket: " + jadwal);
                            System.out.println("Harga Tiket: " + hargaTiket);
                            pembayaranTiket1.setSaldo(pembayaranTiket1.getSaldo() - hargaTiket);

                            pesantiket1 = new pesantiket("ABC123","a",20);
                            pesantiket1.tambahKursi("Nomor Kursi");
                            jadwalPemesanan = jadwal;
                            hargaPemesanan = hargaTiket;
                            pesantiket1.cetakTiket();
                        } else {
                            System.out.println("Kelas tiket tidak valid.");
                        }
                    } else {
                        System.out.println("Kode jadwal tiket tidak valid.");
                    }
                    break;

                case 4:
                    System.out.println("Pembayaran Tiket");
                    if (pesantiket1 != null) {
                        System.out.println("Jadwal Tiket: " + jadwalPemesanan);
                        System.out.println("Harga Tiket: " + hargaPemesanan);
                        boolean pembayaranBerhasil = pembayaranTiket1.bayar();
                        if (pembayaranBerhasil) {
                            pembayaranTiket1.setSaldo(pembayaranTiket1.getSaldo() - hargaPemesanan);
                        }
                    } else {
                        System.out.println("Belum ada tiket yang dipesan.");
                    }
                    break;

                case 5:
                    System.out.println("Terima kasih telah menggunakan Aplikasi KAI Access.");
                    System.exit(0);
                    break;

                default:
                    System.out.println("Input yang Anda Masukan Salah");
                    break;
            }
        }
    }
}