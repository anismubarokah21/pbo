import java.util.ArrayList;
import java.util.Scanner;

public class pesantiket {
    private String kodePemesanan;
    private String namaPenumpang;
    private int jumlahTiket = 20;
    private ArrayList<String> daftarKursi;

    public pesantiket(String kodePemesanan, String namaPenumpang, int jumlahTiket) {
        this.kodePemesanan = kodePemesanan;
        this.namaPenumpang = namaPenumpang;
        this.jumlahTiket = jumlahTiket;
        this.daftarKursi = new ArrayList<String>();
        for (int i = 1; i <= jumlahTiket; i++) {
            daftarKursi.add(String.valueOf(i));
        }
    }

    public String getKodePemesanan() {
        return kodePemesanan;
    }

    public String getNamaPenumpang() {
        return namaPenumpang;
    }

    public int getJumlahTiket() {
        return jumlahTiket;
    }

    public ArrayList<String> getDaftarKursi() {
        return daftarKursi;
    }

    public void tambahKursi(String nomorKursi) {
        if (daftarKursi.size() < jumlahTiket) {
            daftarKursi.add(nomorKursi);
        } else {
            System.out.println("Jumlah kursi yang dipilih sudah mencapai batas maksimum");
        }
    }

    public void hapusKursi(String nomorKursi) {
        if (daftarKursi.contains(nomorKursi)) {
            daftarKursi.remove(nomorKursi);
        } else {
            System.out.println("Kursi sudah dipesan");
        }
    }

    public void cetakTiket() {
        Scanner input = new Scanner(System.in);
        System.out.print("Nama Penumpang    : ");
        String nama = input.nextLine();
        System.out.print("Jumlah Tiket      : ");
        int jumlah = input.nextInt();
        input.nextLine();
        System.out.println("Kode Pemesanan    : " + kodePemesanan);
        System.out.println("Nama Penumpang    : " + nama);
        System.out.println("Jumlah Tiket      : " + jumlah);
        System.out.println("Daftar Kursi      : " + daftarKursi);
        System.out.print("Pilih Kursi       : ");
        String kursi = input.nextLine();
        System.out.println("Kursi yang dipilih: " + kursi);
    }
}