# nomer 1
**Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat.**

Menurut saya algoritma yang saya gunakan pada class KAIAccess merupakan algoritma yang paling sulit dari program yang lain. Program ini menggunakan pendekatan prosedural dalam pengorganisasian kode. Algoritma ini melibatkan penggunaan class dan object untuk mengorganisir dan mengelola logika program. Berikut adalah algoritma dari KAIAccess untuk melihat jadwal tiket kereta, harga tiket kereta, pemesanan tiket kereta, dan pembayaran tiket kereta.

1. Inisialisai variabel 'username', 'password', 'input', 'Authenticator', 'jadwaltiket1', 'hargaTiket1', 'pembayaranTiket1', 'pesantiket1', 'jadwalPemesanan', dan 'hargaPemesanan' dengan nilai awal yang sesuai.
2. Cetak 'Selamat Datang di Aplikasi KAI Access' dan meminta user untuk memasukkan username dan password.
3. panggil method 'authenticate' dari objek 'Authenticator' dengan argumen 'username' dan 'password'. Kemudian memverivikasi apakah user telah memasukkan username dan password yang valid dan apakah user mendapat izin untuk masuk ke dalam sistem.
4. Jika autentikasi berhasil atau bernilai **true** maka program akan melanjutkan ke langkah berikutnya, jika **false** maka user tidak bisa melanjutkan ke langkah berikutnya dan program akan menampilkan 'Username atau password salah! Silakan coba lagi.'
5. Program menampilkan menu pilihan kepada user, seperti 
    - Melihat Jadwal Tiket
    - Melihat Harga Tiket
    - Pemesanan Tiket
    - Pembayaran Tiket
    - Keluar
6. Baca input pilihan dari user menggunakan objek 'input.nextInt'.
7. Gunakan **switch case** untuk memproses pilihan user:
    - Case 1
        
        Jika user memilih 1 maka panggil method 'tampilJadwal' dari objek  'jadwaltiket1' untuk menampilkan jadwal tiket kereta.

    - Case 2

        JIka user memilih 2, panggil method 'tampilharga' dari objek 'hargaTiket1' untuk menampilkan harga tiket sesuai dengan kelas tiket.
    - Case 3

        Jika user memillih 3, minta user memasukkan kode jadwal tiket, masukkan kelas tiket,jika user sudah tepat memasukan kode jadwal tiket dan kelas tiket maka akan muncul pesan pemesanan tiket berhasil, kemudian user di minta memasukkan nama penumpang, dan jumlah tiket yang ingin di pesan. Panggil method 'tambahKursi' dari objek 'pesantiket1' untuk memasukkan nomor kursi. Cetak tiket dengan memanggil method 'cetakTiket' dari objek 'pesantiket1', kemudian user di minta memilih kursi jika sudah akan muncul kursi yang dipilih.
    - Case 4

        Jika user memilih 4, panggil method 'bayar' dari objek 'pembayaranTiket1' untuk memproses pembayaran tiket. Jika pembayaran berhasil akan tampil pesan 'Pembayaran berhasil' dan menaampilkan sisa saldo, jika gagal maka akan tampil pesan 'PIN Salah'.
    - Case 5
        Jika user memilih 5, keluar dari program dan tampil pesan 'Terima kasih telah menggunakan Aplikasi KAI Access.' Jika user memilih angka selain 1-5 maka tampil pesan 'Input yang Anda Masukan Salah'.

Dengan menggunakan switch case program KAIAccess dapat memproses pilihan user dengan cara yang terstruktur dan dapat memilih jalur eksekusi yang sesuai berdasarkan nilai pilihan yang di berikan oleh user.


**Algoritma Pseudocode**

Judul: KAIAccess

Deklarasi:
```
Procedure main():
username, password as String
loggedIn as Boolean
input as Scanner
login as Login
pil as Integer
jadwaltiket1 as jadwaltiket
pesantiket1 as pesantiket
hargaTiket1 as HargaTiket
pembayaranTiket1 as PembayaranTiket
```
implementasi:

    Print "Username: "
    Read username from input
    Print "Password: "
    Read password from input

    login = Create Login object with username "anis" and password "anis123"

    If login.authenticate(username, password) Then
        Set loggedIn as true
        Print "Selamat anda berhasil login!"
    Else
        Print "Username atau password salah."
        Exit program

    While true:
        Print "Selamat Datang di Aplikasi KAI Access"
        Print "1. Melihat Jadwal Tiket"
        Print "2. Melihat Harga Tiket"
        Print "3. Pemesanan Tiket"
        Print "4. Pembayaran Tiket"
        Print "Pilihan: "
        Read pil from input

        Switch pil:
            Case 1:
                Print "Melihat Jadwal Tiket"
                jadwaltiket1.tambahJadwal("KA 123", "Jakarta", "Surabaya", LocalTime.of(8, 0), LocalTime.of(16, 0))
                jadwaltiket1.tambahJadwal("KA 456", "Surabaya", "Jakarta", LocalTime.of(9, 0), LocalTime.of(17, 0))
                jadwaltiket1.tambahJadwal("KA 213", "Bandung", "Banjar", LocalTime.of(14, 0), LocalTime.of(18, 0))
                jadwaltiket1.tambahJadwal("KA 215", "Banjar", "Bandung", LocalTime.of(22, 0), LocalTime.of(3, 0))
                jadwaltiket1.tampilJadwal()
            Case 2:
                Print "Melihat Harga Tiket"
                hargaTiket1.tampilharga()
            Case 3:
                Print "Pemesanan Tiket"
                Print "Masukkan kode jadwal tiket: "
                Read kodeJadwal from input
                jadwal = jadwaltiket1.getJadwalByKode(kodeJadwal)
                If jadwal is not null Then
                    Print "Masukkan kelas tiket: "
                    Read kelasTiket from input
                    HargaTiket = hargaTiket1.getHarga(kelasTiket)
                    If HargaTiket > 0 Then
                        Print "Pemesanan tiket berhasil!"
                        Print "Jadwal Tiket: " + jadwaltiket1
                        Print "Harga Tiket: " + hargaTiket1
                        pesantiket1.tambahKursi("Nomor Kursi")
                    Else
                        Print "Kelas tiket tidak valid."
                    End If
                Else
                    Print "Kode jadwal tiket tidak valid."
                End If
                pesantiket1.cetakTiket()
            Case 4:
                Print "Pembayaran Tiket"
                Print "Masukkan kelas tiket: "
                Read kelasTiket from input
                Print "Jadwal Tiket: " + pesantiket1.getKodePemesanan()
                Print "Harga Tiket: " + hargaTiket1.getHarga(kelasTiket)
                pembayaranTiket1.bayar()
            Default:
    Print "Input yang Anda Masukan Salah"
    End Switch
    End While
    End Procedure


**Algoritma bahasa Java**
```
import java.util.Scanner;
import java.time.LocalTime;

public class KAIAccess {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan username: ");
        String username = input.nextLine();
        System.out.print("Masukkan password: ");
        String password = input.nextLine();

        Authenticator authenticator = new Login("Anis", "anis123");
        boolean isAuthenticated = authenticator.authenticate(username, password);

        if (isAuthenticated) {
            System.out.println("Berhasil Login!");
        } else {
            System.out.println("Username atau password salah! Silakan coba lagi.");
            System.exit(0);
        }
        
        int pil;
        jadwaltiket jadwaltiket1 = new jadwaltiket();
        HargaTiket hargaTiket1 = new HargaTiket();
        PembayaranTiket pembayaranTiket1 = new PembayaranTiket(0.0, 1000000);
        pesantiket pesantiket1 = null;
        Jadwal jadwalPemesanan = null;
        double hargaPemesanan = 0.0;

        while (true) {
            System.out.println("Selamat Datang di Aplikasi KAI Access");
            System.out.println("1. Melihat Jadwal Tiket");
            System.out.println("2. Melihat Harga Tiket");
            System.out.println("3. Pemesanan Tiket");
            System.out.println("4. Pembayaran Tiket");
            System.out.println("5. Keluar");
            System.out.print("Pilihan: ");
            pil = input.nextInt();

            switch (pil) {
                case 1:
                    System.out.println("Melihat Jadwal Tiket");
                    jadwaltiket1.tambahJadwal("KA 123", "Jakarta", "Surabaya", LocalTime.of(8, 0), LocalTime.of(16, 0));
                    jadwaltiket1.tambahJadwal("KA 456", "Surabaya", "Jakarta", LocalTime.of(9, 0), LocalTime.of(17, 0));
                    jadwaltiket1.tambahJadwal("KA 213", "Bandung", "Banjar", LocalTime.of(14, 0), LocalTime.of(18, 0));
                    jadwaltiket1.tambahJadwal("KA 215", "Banjar", "Bandung", LocalTime.of(22, 0), LocalTime.of(3, 0));
                    jadwaltiket1.tampilJadwal();
                    break;

                case 2:
                    System.out.println("Melihat Harga Tiket");
                    hargaTiket1.tampilharga();
                    break;

                case 3:
                    System.out.println("Pemesanan Tiket");
                    System.out.print("Masukkan kode jadwal tiket: ");
                    input.nextLine();
                    String kodeJadwal = input.nextLine();
                    Jadwal jadwal = jadwaltiket1.getJadwalByKode(kodeJadwal);
                    if (jadwal != null) {
                        System.out.print("Masukkan kelas tiket: ");
                        String kelasTiket = input.nextLine();
                        double hargaTiket = hargaTiket1.getHarga(kelasTiket);
                        if (hargaTiket > 0) {
                            System.out.println("Pemesanan tiket berhasil!");
                            System.out.println("Jadwal Tiket: " + jadwal);
                            System.out.println("Harga Tiket: " + hargaTiket);
                            pembayaranTiket1.setSaldo(pembayaranTiket1.getSaldo() - hargaTiket);

                            pesantiket1 = new pesantiket("ABC123","a",20);
                            pesantiket1.tambahKursi("Nomor Kursi");
                            jadwalPemesanan = jadwal;
                            hargaPemesanan = hargaTiket;
                            pesantiket1.cetakTiket();
                        } else {
                            System.out.println("Kelas tiket tidak valid.");
                        }
                    } else {
                        System.out.println("Kode jadwal tiket tidak valid.");
                    }
                    break;

                case 4:
                    System.out.println("Pembayaran Tiket");
                    if (pesantiket1 != null) {
                        System.out.println("Jadwal Tiket: " + jadwalPemesanan);
                        System.out.println("Harga Tiket: " + hargaPemesanan);
                        boolean pembayaranBerhasil = pembayaranTiket1.bayar();
                        if (pembayaranBerhasil) {
                            pembayaranTiket1.setSaldo(pembayaranTiket1.getSaldo() - hargaPemesanan);
                        }
                    } else {
                        System.out.println("Belum ada tiket yang dipesan.");
                    }
                    break;

                case 5:
                    System.out.println("Terima kasih telah menggunakan Aplikasi KAI Access.");
                    System.exit(0);
                    break;

                default:
                    System.out.println("Input yang Anda Masukan Salah");
                    break;
            }
        }
    }
}
```
# nomor 2
**Mampu menjelaskan algoritma dari solusi yang dibuat**

Berikut algoritma source code yang digunakan dalam program

[KAIAccess.java](/uploads/c61b9510a6fe4acc7bdec0eceec81a00/KAIAccess.java)

Source code class yang lain

[Authenticator](https://gitlab.com/anismubarokah21/pbo/-/blob/main/source%20code/Authenticator.java
)

[jadwaltiket](https://gitlab.com/anismubarokah21/pbo/-/blob/main/source%20code/jadwaltiket.java
)

[HargaTiket](https://gitlab.com/anismubarokah21/pbo/-/blob/main/source%20code/HargaTiket.java
)

[pesantiket](https://gitlab.com/anismubarokah21/pbo/-/blob/main/source%20code/pesantiket.java
)

[PembayaranTiket](https://gitlab.com/anismubarokah21/pbo/-/blob/main/source%20code/PembayaranTiket.java
)

# nomor 3
**Mampu menjelaskan konsep dasar OOP**

Berikut adalah konsep dasar dalam OOP:
1. Objek, ialah instansi dari sebuah kelas. Objek memiliki atribut (variabel) yang merepresentasikan keadaan internalnya dan metode (fungsi) yang menggambarkan perilaku atau tindakan yang dapat dilakukan oleh objek tersebut. Objek ini yang bisa membedakan antara objek yang satu dengan objek yang lain.
2. Class, ialah sebuah blueprint atau cetak biru untuk menciptakan objek-objek yang serupa. Kelas mendefinisikan atribut dan metode yang dimiliki oleh objek-objek yang dibuat dari class tersebut. Class ini bisa disebut sebagai kerangka.
3. Atribut, ialah variabel yang menyimpan data atau informasi terkait dengan objek. Atribut ini merupakan data yang membedakan antara objek yang satu dengan objek yang lain.
4. Method, method ini dapat disebut dengan aksi atau tingkah laku ialah hal-hal yang dapat dilakukan objek dari suatu class.

    Contohnya: 
    
    Class: Mobil.

    Objek: Mobil avanza, mobil kijang.

    Atribut: Merk, warna, plat nomor.

    Method: Maju, mundur, belok, berhenti.

Kemudian dalam OOP juga terdapat empat pilar, yakni:
1. Encapsulation, ialah konsep yang memungkinkan penggabungan data dan metode dalam satu entitas yang disebut objek. 
Encapsulation ini memungkinkan data dan fungsi-fungsi yang berhubungan dengan data tersebut untuk diorganisasi dalam satu kesatuan yang disebut dengan class atau object, dan hanya bisa diakses oleh class-class yang sudah ditentukan.
2. Abstraction (abstraksi), abstraksi ini memungkinkan pemodelan objek secara lebih sederhana dengan hanya memperhatikan detail-detail yang penting dan mengabaikan detail yang tidak relevan. Abstraksi ini memungkinkan untuk menyembunyikan detail yang kompleks dari pengguna, sehingga memungkinkan pengguna untuk lebih mudah memahami dan menggunakan aplikasi.
3. Inheritance (Pewarisan), inheritance ini memungkinkan kelas baru (kelas turunan atau subclass) mewarisi atribut dan metode dari kelas yang sudah ada (kelas induk atau superclass), dengan menggunakan pewarisan, kelas turunan dapat memperluas atau memodifikasi perilaku yang sudah ada dalam kelas induk tanpa harus menulis ulang kode yang sama.
4. Polymorphism, Polymorphism ini memungkinkan objek-objek yang berbeda untuk merespons secara berbeda terhadap metode-metode yang sama. Polymorphism dapat dicapai melalui konsep overriding dan overloading. Polymorphism ini juga memungkinkan kita untuk menggunakan satu interface atau nama method untuk merepresentasikan banyak jenis objek yang berbeda.

# nomor 4
**Mampu mendemonstrasikan penggunaan Encapsulation secara tepat**

Dalam proses bisnis yang saya buat yakni KAIAccess dalam semua classnya saya menggunakan konsep encapsulation, disini saya akan melampirkan salah satu dari beberapa class yang menggunakan konsep encapsulation yakni pada class Authenticator (Login). Encapsulation ialah suatu cara untuk menggabungkan data dan fungsi terkait ke dalam suatu objek atau class. Tujuan dari encapsulation sendiri ialah untuk melindungi data agar tidak dapat diakses atau dimodifikasi secara langsung dari luar objek atau class. Hal ini membantu dalam menerapkan prinsip-prinsip keamanan dan memastikan bahwa data hanya dapat diakses atau dimodifikasi melalui metode yang ditentukan, sehingga mencegah perubahan yang tidak sah atau tidak diinginkan dari luar class tersebut. Encapsulation ini berhubungan dengan Acces Modifier, access  modifier ini digunakan untuk mengatur tingkat aksebilitas atribut dan method dalam suatu class. Access Modifier ini terbagi menjadi beberapa jenis yakni, public, private, protected dan default. Public ini dapat diakses dari mana saja baik dari dalam class itu sendiri, class lain dalam paket yang sama, maupun class di paket lain. Private ini hanya dapat diakses oleh class yang sama,  tidak dapat diakses dari class lain, termasuk class turunan (subclass), penggunaan private ini membantu menerapkan konsep encapsulation dengan membatasi akses langsung ke data tersebut. Protected ini dapat diakses oleh class yang sama, class turunan (subclass), dan class lain dalam paket yang sama namun, tidak dapat diakses dari class di paket yang berbeda, kecuali jika class tersebut merupakan turunan (subclass) dari class tersebut. Default ini hanya dapat diakses oleh class dalam package yang sama.

Konsep encapsulation digunakan dengan mendeklarasikan data member (username dan password) sebagai private dalam class Login. Atribut tersebut dideklarasikan sebagai private yang berarti hanya dapat diakses dan dimodifikasi dalam class tersebut. Dengan mendeklarasikan atribut sebagai private, kita dapat membatasi akses langsung dari luar class. Untuk mengakses atau memodifikasi nilai atribut tersebut, kita perlu menggunakan metode atau fungsi yang disediakan dalam class itu sendiri. Dalam hal ini, kita dapat menggunakan konstruktor 'Login' untuk mengatur nilai username dan password saat objek dibuat, dan menggunakan metode authenticate untuk memverifikasi kecocokan username dan password.

Berikut source code class Authenticator(Login):
```
import java.util.Scanner;

abstract class Authenticator {
    public abstract boolean authenticate(String username, String password);
}

class Login extends Authenticator {
    private String username;
    private String password;

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public boolean authenticate(String username, String password) {
        return this.username.equals(username) && this.password.equals(password);
    }
}
```
# nomor 5
**Mampu mendemonstrasikan penggunaan Abstraction secara tepat**

Abstraction ini memungkinkan pemodelan objek secara lebih sederhana dengan hanya memperhatikan detail-detail yang penting dan mengabaikan detail yang tidak relevan. Abstraksi ini juga memungkinkan untuk menyembunyikan detail yang kompleks dari pengguna, sehingga memungkinkan pengguna untuk lebih mudah memahami dan menggunakan aplikasi. Class abstrak mendefinisikan metode-metode abstrak, yang merupakan metode tanpa implementasi konkret, tetapi hanya menyediakan penandanya. Metode abstrak harus diimplementasikan oleh class turunannya, yang memberikan implementasi spesifik dari metode tersebut. Konsep abstraksi ini membantu kita dalam memodelkan dunia nyata yang kompleks ke dalam entitas yang lebih terorganisir dan terstruktur, ini memungkinkan pengembang untuk fokus pada fitur dan perilaku yang penting dari objek sambil menyembunyikan detail implementasi yang kompleks di balik interface yang lebih sederhana. Dengan cara ini, abstraksi memungkinkan pengembangan yang lebih terstruktur, pemeliharaan yang lebih mudah, dan peningkatan modularitas dalam pembuatan program.

Dalam program yang sudah saya buat, terdapat konsep abstraksi yang digunakan dengan membuat kelas abstrak Authenticator. Kelas abstrak ini menyediakan metode abstrak authenticate(), yang harus diimplementasikan oleh kelas turunannya. 

Berikut source codenya:
```
import java.util.Scanner;

abstract class Authenticator {
    public abstract boolean authenticate(String username, String password);
}

class Login extends Authenticator {
    private String username;
    private String password;

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public boolean authenticate(String username, String password) {
        return this.username.equals(username) && this.password.equals(password);
    }
}
```

# nomor 6
**Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat**

Dalam proses bisnis yang saya buat yakni KAI Access, terdapat konsep inheritance dan Polymorphism pada class Login.

**Inheritance**

Inheritance ini memungkinkan kelas baru (kelas turunan atau subclass) mewarisi atribut dan metode dari kelas yang sudah ada (kelas induk atau superclass), dengan menggunakan pewarisan, kelas turunan dapat memperluas atau memodifikasi perilaku yang sudah ada dalam kelas induk tanpa harus menulis ulang kode yang sama. Dalam contoh class Login mewarisi kelas abstrak Authenticator. Hal ini menunjukkan pewarisan, di mana kelas turunan (Login) dapat mewarisi properti dan metode dari kelas induk (Authenticator). Dengan mewarisi dari kelas abstrak, kelas Login wajib mengimplementasikan metode abstrak authenticate() yang ditentukan dalam kelas abstrak tersebut. Dengan menggunakan inheritance, class Login dapat memperluas atau menyesuaikan perilaku yang sudah ada di class Authenticator sesuai dengan kebutuhan kelas tersebut.

**Polymorphism**

Polymorphism ini memungkinkan objek-objek yang berbeda untuk merespons secara berbeda terhadap metode-metode yang sama. Polymorphism dapat dicapai melalui konsep overriding dan overloading. Polymorphism ini juga memungkinkan kita untuk menggunakan satu interface atau nama method untuk merepresentasikan banyak jenis objek yang berbeda. Konsep Polymorphism digunakan pada objek Authenticator untuk merujuk pada objek kelas turunannya, yaitu Login. Polimorfisme memungkinkan kita menggunakan objek dari kelas turunan (Login) sebagai pengganti objek kelas abstrak (Authenticator), dan memanggil metode yang sama (authenticate()) pada objek tersebut. Polymorphism terlihat melalui penggunaan metode authenticate dalam class Login. Meskipun variabel authenticator dideklarasikan sebagai objek class Authenticator, namun pada saat runtime, objek yang sebenarnya yang diinisialisasi adalah objek class Login. Hal ini memungkinkan untuk memanggil metode authenticate melalui objek authenticator, dan metode yang dijalankan adalah metode authenticate yang diimplementasikan di class Login, bukan di class Authenticator. Dengan menggunakan Polymorphism ini dapat membantu dalam membuat program yang lebih fleksibel, dapat diperluas, dan memudahkan pengelolaan objek dengan tipe yang berbeda namun memiliki kesamaan dalam perilaku.


Berikut Source codenya:
```
import java.util.Scanner;

abstract class Authenticator {
    public abstract boolean authenticate(String username, String password);
}

class Login extends Authenticator {
    private String username;
    private String password;

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public boolean authenticate(String username, String password) {
        return this.username.equals(username) && this.password.equals(password);
    }
}
```

# nomor 7
**Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP**


Beberapa use case yang saya jadikan program untuk aplikasi KAI Access:
1. Use case Login, untuk mengautentikasi pengguna agar melakukan login terlebih dahulu pada aplikasi, diimplementasikan pada class Authenticator.
2. Use case Melihat jadwal tiket, untuk melihat jadwal keberangkatan kereta api yang tersedia.
3. Use case Melihat harga tiket, untuk melihat harga tiket kereta api berdasarkan kelas yang dipilih. Pengguna dapat memilih kelas eksekutif, bisnis, atau ekonomi. 
4. Use case Pemesanan tiket, untuk memesan tiket kereta api. Pengguna diminta untuk memasukkan kode jadwal tiket yang ingin dipesan. Untuk pengimplementasiannya saya gunakan pada class pesantiket. Pada **class pesantiket** terdapat atribut (kodePemesanan, namaPenumpang, jumlahTiket, dan daftarKursi). kodePemesanan dan namaPenumpang merupakan variabel bertipe data String, sedangkan jumlahTiket merupakan variabel bertipe data integer, dan daftarKursi merupakan variabel bertipe ArrayList. Class pesantiket ini memiliki 3 method yaitu method pesantiket, tambahKursi, dan hapusKursi.
5. Use case Pembayaran tiket, untuk melakukan pembayaran tiket kereta api yang sudah dipesan.


# nomor 8
**Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table**

**Class Diagram**
![](https://gitlab.com/anismubarokah21/pbo/-/raw/main/class%20diagram/class_diagram_KAI.png)


**Use Case Table**
| NO | Actor | Bisa |
| --- | ------ | -----|
| 1.  | User  | Login ke dalam akun  |
| 2.  | User  | Logout dari akun    |
| 3.  | User  | Memesan tiket    |
| 4.  | User  | Mengetahui jadwal keberangkatan    |
| 5.  | User  | Melihat harga tiket    |
| 6.  | User  | Memilih tempat duduk    |
| 7.  | User  | Melakukan pembayaran tiket    |
| 8.  | User  | Menyimpan data pemesanan tiket    |
| 9.  | User  | Melakukan refund tiket    |
| 10. | User  | Mengetahui informasi terbaru tentang kereta api    |
| 11. | User  | Melihat kereta api yang beroperasi    |
| 12. | User  | Memantau status pemesanan tiket    |
| 13. | User  | Mengakses informasi tentang stasiun kereta api    |
| 14. | User  | Menghubungi customer service KAI    |
| 15. | User  | Mengakses promo dan diskon    |
| 16. | User  | Menyimpan riwayat perjalanan    |
| 17. | User  | Memanfaatkan fitur QR code    |
| 18. | User  | Mencari kereta berdasarkan stasiun asal dan tujuan    |
| 19. | User  | Melihat profil pengguna    |
| 20. | User  | Mengubah atau memperbarui profil pengguna    |
| 21. | User  | Mengubungi layanan pelanggan    |
| 22. | User  | Memberikan umpan balik dan rating pada aplikasi KAI Access    |
| 23. | Manajemen (Admin)  | Melihat daftar pengguna yang terdaftar dalam aplikasi   |
| 24  | Manajemen (Admin)  | Menambahkan pengguna baru ke dalam aplikasi dengan menyediakan informasi seperti username, password, dan data pengguna lainnya   |
| 25  | Manajemen (Admin)  | Menghapus pengguna yang tidak diperlukan dari aplikasi    |
| 26. | Manajemen (Admin)  | Menambahkan, mengubah, atau menghapus jadwal tiket kereta api dalam sistem.    |
| 27. | Manajemen (Admin)  | Menambahkan, mengubah, atau menghapus harga tiket untuk berbagai kelas dan jenis tiket    |
| 28. | Manajemen (Admin)  | Mengelola daftar kursi yang tersedia untuk setiap jadwal tiket    |
| 29. | Manajemen (Admin)  | Melihat laporan penjualan tiket kereta api, termasuk jumlah tiket terjual, pendapatan, dan statistik lainnya   |
| 30. | Manajemen (Admin)  | Membuat dan mengelola promosi atau penawaran khusus untuk tiket kereta api   |
| 31. | Manajemen (Admin)  | Mengelola sistem pembayaran    |
| 32. | Manajemen (Admin)  | Mengelola notifikasi kepada pengguna terkait informasi terkini, perubahan jadwal, atau penawaran khusus   |
| 33. | Manajemen (Admin)  | Mengelola profil aplikasi    |
| 34. | Manajemen (Admin)  | Mengatur dan mengelola izin akses pengguna dalam aplikasi   |
| 35. | Manajemen (Admin)  | Melakukan pemeliharaan sistem    |
| 36. | Manajemen (Admin)  | Mengelola layanan pelanggan    |
| 37. | Manajemen (Admin)  | Mengelola data pengguna   |
| 38. | Manajemen (Admin)  | Mengelola keamanan aplikasi    |
| 39. | Manajemen (Admin)  | Mengelola pengaturan aplikasi    |
| 40. | Manajemen (Admin)  | Mengelola kebijakan refund dan pembatalan   |
| 41. | Manajemen (Admin)  | Mengelola sistem pengiriman tiket |
| 42. | Manajemen (Admin)  | Mengelola data statistik  |
| 43. | Manajemen (Admin)  | Mengelola riwayat transaksi  |

# nomor 9
**Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video**


[Link Youtube](https://youtu.be/VeKHny8isfg)

# nomor 10
**Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)**

![vidio_output_Tugas_Besar_PBO_Anis_Mubarokah](/uploads/50f59ae7c0ebddc15c6affc8b2955d20/vidio_output-Tugas_Besar_PBO-Anis_Mubarokah.mp4)

[Link Youtube](https://youtu.be/VeKHny8isfg)
