import java.util.HashMap;
import java.util.Map;

public class HargaTiket {
    private Map<String, Double> hargaMap;

    public HargaTiket() {
        hargaMap = new HashMap<>();
        hargaMap.put("Ekonomi (Anak)", 50000.0);
        hargaMap.put("Ekonomi (Dewasa)", 68000.0);
        hargaMap.put("Bisnis (Anak)", 100000.0);
        hargaMap.put("Bisnis (Dewasa)", 150000.0);
        hargaMap.put("Eksekutif (Anak)", 200000.0);
        hargaMap.put("Eksekutif (Dewasa)", 250000.0);
    }

    public void tampilharga() {
        System.out.println("Harga Tiket:");
        for (Map.Entry<String, Double> entry : hargaMap.entrySet()) {
            String kelas = entry.getKey();
            double harga = entry.getValue();
            System.out.println("- " + kelas + ": Rp " + harga);
        }
    }
    public double getHarga(String kelasTiket) {
        return hargaMap.getOrDefault(kelasTiket, 0.0);
    }
}