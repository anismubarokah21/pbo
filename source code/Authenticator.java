import java.util.Scanner;

abstract class Authenticator {
    public abstract boolean authenticate(String username, String password);
}

class Login extends Authenticator {
    private String username;
    private String password;

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public boolean authenticate(String username, String password) {
        return this.username.equals(username) && this.password.equals(password);
    }
}