import java.time.LocalTime;
import java.util.ArrayList;

public class jadwaltiket {
    private ArrayList<Jadwal> jadwalList;
    private Jadwal jadwal;
    private double HargaTiket;

    public jadwaltiket() {
        jadwalList = new ArrayList<>();
    }

    public void tambahJadwal(String nomorKereta, String stasiunAsal, String stasiunTujuan, LocalTime waktuBerangkat, LocalTime waktuTiba) {
        Jadwal jadwal = new Jadwal(nomorKereta, stasiunAsal, stasiunTujuan, waktuBerangkat, waktuTiba);
        jadwalList.add(jadwal);
    }

    public void tampilJadwal() {
        if (jadwalList.isEmpty()) {
            System.out.println("Belum ada jadwal tiket.");
        } else {
            System.out.println("Daftar Jadwal Tiket:");
            for (Jadwal jadwal : jadwalList) {
                System.out.println("Kode Jadwal : " + jadwal.getNomorKereta());
                System.out.println("Asal        : " + jadwal.getStasiunAsal());
                System.out.println("Tujuan      : " + jadwal.getStasiunTujuan());
                System.out.println("Jam Berangkat : " + jadwal.getWaktuBerangkat());
                System.out.println("Jam Tiba   : " + jadwal.getWaktuTiba());
                System.out.println("-----------------------------");
            }
        }
    }
    public Jadwal getJadwalByKode(String kodeJadwal) {
        for (Jadwal jadwal : jadwalList) {
            if (jadwal.getNomorKereta().equalsIgnoreCase(kodeJadwal)) {
                return jadwal;
            }
        }
        return null;
    }
    
    public static void main(String[] args) {
        jadwaltiket jadwalTiket = new jadwaltiket();
        jadwalTiket.tambahJadwal("KA 123", "Jakarta", "Surabaya", LocalTime.of(8, 0), LocalTime.of(16, 0));
        jadwalTiket.tambahJadwal("KA 456", "Surabaya", "Jakarta", LocalTime.of(9, 0), LocalTime.of(17, 0));
        jadwalTiket.tambahJadwal("KA 213", "Bandung", "Banjar", LocalTime.of(14, 0), LocalTime.of(18, 0));
        jadwalTiket.tambahJadwal("KA 215", "Banjar", "Bandung", LocalTime.of(22, 0), LocalTime.of(3, 0));
        jadwalTiket.tampilJadwal();
    }
}

class Jadwal {
    private String nomorKereta;
    private String stasiunAsal;
    private String stasiunTujuan;
    private LocalTime waktuBerangkat;
    private LocalTime waktuTiba;
    
    public Jadwal(String nomorKereta, String stasiunAsal, String stasiunTujuan, LocalTime waktuBerangkat, LocalTime waktuTiba) {
        this.nomorKereta = nomorKereta;
        this.stasiunAsal = stasiunAsal;
        this.stasiunTujuan = stasiunTujuan;
        this.waktuBerangkat = waktuBerangkat;
        this.waktuTiba = waktuTiba;
    }
    public String getNomorKereta() {
        return nomorKereta;
    }
    public String getStasiunAsal() {
        return stasiunAsal;
    }
        public String getStasiunTujuan() {
        return stasiunTujuan;
    }
    public LocalTime getWaktuBerangkat() {
        return waktuBerangkat;
    }
    public LocalTime getWaktuTiba() {
        return waktuTiba;
    }
    @Override
    public String toString() {
        return nomorKereta + " | " + stasiunAsal + " - " + stasiunTujuan + " | Berangkat: " + waktuBerangkat + ", Tiba: " + waktuTiba;
    }
}